---
layout: page
---

> This website is still a work in progress ({{ site.time | date: "%b %d, %Y" }})

# Installation

---

## Using docker-compose

You can install vTrack in no time using the docker-compose file from the project.

### Prerequisites

In order to complete the steps in this guide, you will need the following:

- a [Docker environment](https://docs.docker.com/install/).
- [docker-compose](https://docs.docker.com/compose/install/).

### Run vTrack

Download the `docker-compose.yml` file from the project:
```sh
wget https://gitlab.com/vtrack/vtrack-server/raw/master/docker-compose.yml
```

Run the `docker-compose.yml` file:
```sh
docker-compose up
```

---

## From source

### Prerequisites

In order to complete the steps in this guide, you will need the following:

- [Task, a task runner / build tool](https://taskfile.org/).
- a [Golang development environment](https://golang.org/doc/install).
- a [NodeJS development environment](https://nodejs.org/en/download/).
- a MySQL/MariaDB server, with an empty database.

### Step 1 - Build the vTrack server

Clone the vtrack server and change into the project directory:

```sh
go get gitlab.com/vtrack/vtrack-server
cd $GOPATH/src/gitlab.com/vtrack/vtrack-server
```

Build the server binary using the task runner:

```sh
task server:build
```

At this point you have created the server binary, we will now build the webapp

### Step 2 - Build the vTrack webapp

Clone the vtrack webapp and change into the project directory:

```sh
git clone https://gitlab.com/vtrack/vtrack-webapp.git
cd vtrack-webapp
```

Install project dependencies using the node package manager:

```sh
npm install
```

Build the webapp:

```sh
npm run build
```

At this point you have created the webapp files, we will now put the server and the webapp together, and configure the application for the first start.

### Step 3 - Setup vTrack for first start

Create the application working directories:
```sh
mkdir -p /opt/vtrack/{public,data}
```

Move the server binary and configuration file to the application working directory:
```sh
cp $GOPATH/src/gitlab.com/vtrack/vtrack-server/{vtrack,config.json} /opt/vtrack/
```

Move the webapp files to the application public working directory:
```sh
cp -r vtrack-webapp/dist/* /opt/vtrack/public/
```

Change the database connection information in the `config.json` file using your favorite text editor:
```sh
nano `config.json`
```

After starting the server binary, vTrack should be listening at [`localhost:4000`](https://localhost:4000) by default:
```sh
cd /opt/vtrack
./vtrack
```
